import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

import static utilities.DriverFactory.open;

@RunWith(value = Parameterized.class)
public class NewAccountDDT {
    WebDriver driver;
    private String name, email, phone, gender, password, country;
    private boolean weeklyEmail, monthlyEmail, occasionalEmail;
    private WebElement nameElement, emailElement , phoneElement , passwordElement , verifyPasswordElement , countryElement , maleRadio , femaleRadio , weeklyCheckbox, monthlyCheckbox , occasionalCheckbox, submitButton;

    @Before
    public void setUp() {

        // Navigate to Account Management Page > Click On Create Account
        driver = open("Chrome");
        driver.get("http://sdettraining.com/trguitransactions/AccountManagement.aspx");
        driver.findElement(By.xpath("//*[@id='ctl01']/div[3]/div[2]/div/div[2]/a")).click();
    }

    //This is a test method
    @Test
    public void newAccountTest() {

        System.out.println("New record: " + name + " " + email + " " + phone);

        // Fill out the form
        defineWebElements(driver);
        nameElement.sendKeys(name);
        emailElement.sendKeys(email);
        phoneElement.sendKeys(phone);
        passwordElement.sendKeys(password);
        verifyPasswordElement.sendKeys(password);
        new Select(countryElement).selectByVisibleText(country);

        //Radio button
        if (gender.equalsIgnoreCase("female")) { femaleRadio.click(); }
        else { maleRadio.click(); }

        //Check box
        if (weeklyEmail) { if (!weeklyCheckbox.isSelected()) { weeklyCheckbox.click(); }
        } else { if (weeklyCheckbox.isSelected()) { weeklyCheckbox.click(); } }

        if (monthlyEmail) { if (!monthlyCheckbox.isSelected()) { monthlyCheckbox.click(); }
        } else { if (monthlyCheckbox.isSelected()) { monthlyCheckbox.click(); } }

        if (occasionalEmail) { if (!occasionalCheckbox.isSelected()) { occasionalCheckbox.click(); }
        } else { if (occasionalCheckbox.isSelected()) { occasionalCheckbox.click(); } }

        submitButton.click();
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    //This method is designed to pass parameters into the class via constructor
    @Parameters
    public static List<String[]> getData() {
        return utilities.CSV.get("C:\\Files\\UserAccounts.csv");
    }

    public NewAccountDDT(String name, String email, String phone, String gender, String password, String country, String weeklyEmail, String monthlyEmail, String occasionalEmail) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.gender = gender;
        this.password = password;
        this.country = country;
        if (weeklyEmail.equals("TRUE")) { this.weeklyEmail = true; }
        else { this.weeklyEmail = false; }

        if (monthlyEmail.equals("TRUE")) { this.monthlyEmail = true; }
        else { this.monthlyEmail = false; }

        if (occasionalEmail.equals("TRUE")) { this.occasionalEmail = true; }
        else { this.occasionalEmail = false; }
    }

    private void defineWebElements(WebDriver driver) {
        nameElement = driver.findElement(By.name("ctl00$MainContent$txtFirstName"));
        emailElement = driver.findElement(By.id("MainContent_txtEmail"));
        phoneElement = driver.findElement(By.xpath("//*[@id='MainContent_txtHomePhone']"));
        passwordElement = driver.findElement(By.cssSelector("input[type='password'][id='MainContent_txtPassword']"));
        verifyPasswordElement = driver.findElement(By.name("ctl00$MainContent$txtVerifyPassword"));
        countryElement = driver.findElement(By.id("MainContent_menuCountry"));
        maleRadio = driver.findElement(By.id("MainContent_Male"));
        femaleRadio = driver.findElement(By.cssSelector("input[name='ctl00$MainContent$Gender'][value='Female']"));
        weeklyCheckbox = driver.findElement(By.id("MainContent_checkWeeklyEmail"));
        monthlyCheckbox = driver.findElement(By.name("ctl00$MainContent$checkMonthlyEmail"));
        occasionalCheckbox = driver.findElement(By.id("MainContent_checkUpdates"));
        submitButton = driver.findElement(By.id("MainContent_btnSubmit"));
    }
}
