import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static utilities.DriverFactory.open;

public class ATagsTest {
    WebDriver driver;

    @Test
    public void LoginElementsPresentTest() {
        boolean createAccountPresent = false;
        boolean loginEmailBox = driver.findElement(By.id("MainContent_txtUserName")).isDisplayed();
        List<WebElement> aElements = driver.findElements(By.tagName("a"));
        for(WebElement aElement: aElements) {
            System.out.println(aElement.getText());
            if (aElement.getText().equals("CREATE ACCOUNT")) {
                createAccountPresent = true;
            }
        }

        Assert.assertTrue(createAccountPresent);
    }

    @BeforeMethod
    public void setUp() {
        String weburl = "http://sdettraining.com/trguitransactions/AccountManagement.aspx";
        driver = open("chrome");
        driver.get(weburl);
    }

    @AfterMethod
    public void tearDown() {
        driver.close();
    }

}
