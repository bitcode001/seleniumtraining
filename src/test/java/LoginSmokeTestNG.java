import demos.Listener_Demo;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import static utilities.DriverFactory.open;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

public class LoginSmokeTestNG {
    WebDriver driver;

    @Test
    public void LoginSmokeTest() {
        boolean loginEmailBox = driver.findElement(By.id("MainContent_txtUserName")).isDisplayed();
        System.out.println(loginEmailBox);
        //Assert.assertTrue(loginEmailBox, "Email textbox present");
        Assert.assertTrue(loginEmailBox);
    }

    @BeforeMethod
    public void setUp() {
        String weburl = "http://sdettraining.com/trguitransactions/AccountManagement.aspx";
        driver = open("chrome");
        driver.get(weburl);
    }

    @AfterMethod
    public void tearDown() {
        driver.close();
    }
}
