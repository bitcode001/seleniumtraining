package PageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class AccountPage {

    public AccountPage(WebDriver driver) {
        this.driver = driver;
    }

    WebDriver driver;

    public void setName(String name) {
        driver.findElement(By.name("ctl00$MainContent$txtFirstName")).sendKeys(name);
    }

    public void setEmail(String email) {
        driver.findElement(By.id("MainContent_txtEmail")).sendKeys(email);
    }

    public void setPhone(String phone) {
        driver.findElement(By.xpath("//*[@id='MainContent_txtHomePhone']")).sendKeys(phone);
    }

    public void setPassword(String password) {
        driver.findElement(By.cssSelector("input[type='password'][id='MainContent_txtPassword']")).sendKeys(password);
    }

    public void setVerifyPassword(String password) {
        driver.findElement(By.name("ctl00$MainContent$txtVerifyPassword")).sendKeys(password);
    }

    public void setGender(String gender) {
        WebElement maleRadio = driver.findElement(By.id("MainContent_Male"));
        WebElement femaleRadio = driver.findElement(By.cssSelector("input[name='ctl00$MainContent$Gender'][value='Female']"));
        if (gender.equalsIgnoreCase("female")) { femaleRadio.click(); }
        else { maleRadio.click(); }
    }

    public void setCountry(String country) {
        new Select(driver.findElement(By.id("MainContent_menuCountry"))).selectByVisibleText(country);
    }

    public void setFrequency(String frequency) {
        if (frequency.equalsIgnoreCase("monthly")) {
            driver.findElement(By.name("ctl00$MainContent$checkMonthlyEmail")).click();
        } else if(frequency.equalsIgnoreCase("weekly")) {
            driver.findElement(By.name("ctl00$MainContent$checkWeeklyEmail")).click();
        }
    }

    public void clickSubmit() {
        driver.findElement(By.id("MainContent_btnSubmit")).click();
    }
}
