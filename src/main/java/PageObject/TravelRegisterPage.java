package PageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class TravelRegisterPage {

    WebDriver driver;

    public TravelRegisterPage(WebDriver driver) {
        this.driver = driver;
    }

    public void setFirstName(String firstName) {
        driver.findElement(By.name("firstname")).sendKeys(firstName);
    }

    public void setLastName(String lastName) {
        driver.findElement(By.name("lastname")).sendKeys(lastName);
    }

    public void setPhone(String phone) {
        driver.findElement(By.name("phone")).sendKeys(phone);
    }

    public void setEmail(String email) {
        driver.findElement(By.cssSelector("input[name='email']")).sendKeys(email);
    }

    public void setPassword(String password) {
        driver.findElement(By.cssSelector("input[name='password']")).sendKeys(password);
    }

    public void setConfirmPassword(String confirmPassword) {
        driver.findElement(By.cssSelector("input[name='confirmpassword']")).sendKeys(confirmPassword);
    }

    public void clickConfirm() {
        driver.findElement(By.cssSelector("button[type='submit']")).click();
    }

}
