package demos;
import java.util.List;

import static utilities.CSV.get;

public class DataReaders {

    public static void main(String[] args) {
        readCSV();
    }

    public static void readCSV() {
        String filename = "C:\\Files\\UserAccounts.csv";
        List<String[]> records = get(filename);
        for (String[] record: records) {
            for (String field : record) {
                System.out.println(field);
            }
        }
    }
}
