package demos;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static utilities.DriverFactory.open;

public class TravelRegister {

    WebDriver driver;

    @BeforeMethod
    public void setUp() throws InterruptedException {
        driver = open("chrome");
        driver.get("https://www.phptravels.net");
        driver.findElement(By.xpath("//*[@id='collapse']//*[@id='li_myaccount']/a")).click();
        driver.findElement(By.xpath("//*[@id='collapse']//*[@id='li_myaccount']/ul/li[2]/a")).click();
    }

    @Test
    public void createAccount() {

        //fill out the form
        driver.findElement(By.name("firstname")).sendKeys("Apple");
        driver.findElement(By.name("lastname")).sendKeys("Tree");
        driver.findElement(By.name("phone")).sendKeys("0433356785");
        driver.findElement(By.cssSelector("input[name='email']")).sendKeys("applewelly@gmail.com");
        driver.findElement(By.cssSelector("input[name='password']")).sendKeys("123456");
        driver.findElement(By.name("confirmpassword")).sendKeys("123456");
        driver.findElement(By.cssSelector("button[type='submit']")).click();
    }
}