package demos;

import PageObject.AccountPage;
import PageObject.HomePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.*;

import static utilities.DriverFactory.open;

public class CreateAccountPOM {
    WebDriver driver;

    @BeforeMethod
    public void setUp() {
        driver = open("chromedriver");
        driver.get("http://sdettraining.com/trguitransactions/AccountManagement.aspx");
    }

    @Test
    public void createAccount() {

        // Navigate to Account Management Page > Click On Create Account
        HomePage homePage = new HomePage(driver);
        homePage.createAccount();

        // Fill out the form
        AccountPage accountPage = new AccountPage(driver);
        accountPage.setName("Apple Tree");
        accountPage.setEmail("AppleTree@outlook.com");
        accountPage.setPhone("0455677645");
        accountPage.setPassword("thispass");
        accountPage.setVerifyPassword("thispass");
        accountPage.setGender("Female");
        accountPage.setCountry("Brazil");
        accountPage.setFrequency("monthly");
        accountPage.clickSubmit();

        // Get confirmation
        String conf = driver.findElement(By.id("MainContent_lblTransactionResult")).getText();
        Assert.assertTrue(conf.contains("successfully"));
        System.out.println("Confirmation: " + conf);
    }

    @Test
    public void checkCreateAccount() {

        // Navigate to Account Management Page > Click On Create Account
        HomePage homePage = new HomePage(driver);
        homePage.createAccount();
    }

    @AfterMethod
    public void tearDown() {
        // Close the browser
        driver.close();
    }
}
