package demos;

import PageObject.TravelRegisterPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static utilities.DriverFactory.open;

public class TravelRegisterPOM {

    WebDriver driver;

    @BeforeMethod
    public void setUp() {
        driver = open("chrome"); //Use Chrome
        driver.manage().window().maximize(); //Maximize the window
        driver.get("https://www.phptravels.net"); //Go to the URL
        driver.findElement(By.xpath("//*[@id='collapse']//*[@id='li_myaccount']/a")).click(); //Click MY ACCOUNT
        driver.findElement(By.xpath("//*[@id='collapse']//*[@id='li_myaccount']/ul/li[2]/a")).click(); //Click Sign Up
    }

    @Test
    public void createAccount() {

        //fill out the form
        TravelRegisterPage travelRegisterPage = new TravelRegisterPage(driver);
        travelRegisterPage.setFirstName("Bingo");
        travelRegisterPage.setLastName("Jiang");
        travelRegisterPage.setPhone("0933422134");
        travelRegisterPage.setEmail("applewelly@gmail.com");
        travelRegisterPage.setPassword("123321");
        travelRegisterPage.setConfirmPassword("123321");
        travelRegisterPage.clickConfirm();

        WebDriverWait waitForConfMsg = new WebDriverWait(driver, 5);
        waitForConfMsg.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"body-section\"]/div/div[1]/div/div[1]/h3")));
        String conf = driver.findElement(By.xpath("//*[@id=\"body-section\"]/div/div[1]/div/div[1]/h3")).getText();
        System.out.println("Confirmation: " + conf);
    }
}
