package demos;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import sun.font.TrueTypeFont;
import utilities.DriverFactory;

import static utilities.DriverFactory.open;

public class NewAccount {
    public static void main(String[] args) {
        WebDriver driver;
        String name = "Apple Tree";
        String email = "AppleTree@outlook.com";
        String password = "thispass";
        String country = "Germany";
        String browserType = "chrome";
        String gender = "Female";
        String phoneNumber = "0455677645";
        Boolean weeklyEmail;
        Boolean MonthlyEmail = true;
        Boolean occassionalEmail;

        // 1. Select WebDriver
        driver = open(browserType);

        // 2. Navigate to Account Management Page > Click On Create Account
        driver.get("http://sdettraining.com/trguitransactions/AccountManagement.aspx");
        //driver.findElement(By.linkText("Create")).click();
        driver.findElement(By.xpath("//*[@id='ctl01']/div[3]/div[2]/div/div[2]/a")).click();

        // Define web elements
        WebElement nameElement = driver.findElement(By.name("ctl00$MainContent$txtFirstName"));
        WebElement emailElement = driver.findElement(By.id("MainContent_txtEmail"));
        WebElement phoneElement = driver.findElement(By.xpath("//*[@id='MainContent_txtHomePhone']"));
        WebElement passwordElement = driver.findElement(By.cssSelector("input[type='password'][id='MainContent_txtPassword']"));
        WebElement verifyPasswordElement = driver.findElement(By.name("ctl00$MainContent$txtVerifyPassword"));
        WebElement countryElement = driver.findElement(By.id("MainContent_menuCountry"));
        WebElement maleRadio = driver.findElement(By.id("MainContent_Male"));
        WebElement femaleRadio = driver.findElement(By.cssSelector("input[name='ctl00$MainContent$Gender'][value='Female']"));
        WebElement monthlyCheckbox = driver.findElement(By.name("ctl00$MainContent$checkMonthlyEmail"));
        WebElement submitButton = driver.findElement(By.id("MainContent_btnSubmit"));

        // Fill out the form
        nameElement.sendKeys(name);
        emailElement.sendKeys(email);
        phoneElement.sendKeys(phoneNumber);
        passwordElement.sendKeys(password);
        verifyPasswordElement.sendKeys(password);
        new Select(countryElement).selectByVisibleText(country);

        //Radio button
        if (gender.equalsIgnoreCase("female")) { femaleRadio.click(); }
        else { maleRadio.click(); }

        //Check box
        if (MonthlyEmail) {
            if (!monthlyCheckbox.isSelected()) {
                monthlyCheckbox.click();
            }
        } else {
            if (monthlyCheckbox.isSelected()) {
                monthlyCheckbox.click();
            }
        }

        submitButton.click();

        // 4. Get confirmation
        String conf = driver.findElement(By.id("MainContent_lblTransactionResult")).getText();
        if (conf.contains("Customer information added successfully")) {
            System.out.println("Confirmation: " + conf);
        } else {
            System.out.println("Test failed");
        }

        // 5. Close the browser
        driver.close();
    }

}
