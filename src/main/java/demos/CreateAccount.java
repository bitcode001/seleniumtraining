package demos;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateAccount {

    public static void main(String[] args) {
        // 1. Create WebDriver
//        System.setProperty("webdriver.gecko.driver", "C:\\ChromeDriver\\geckodriver.exe");
//        WebDriver driver = new FirefoxDriver();

        System.setProperty("webdriver.chrome.driver", "C:\\ChromeDriver\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        // 2. Navigate to Account Management Page > Click On Create Account
        driver.get("http://sdettraining.com/trguitransactions/AccountManagement.aspx");
        //driver.findElement(By.linkText("Create")).click();
        driver.findElement(By.xpath("//*[@id=\"ctl01\"]/div[3]/div[2]/div/div[2]/a")).click();

        // 3. Fill out the form
        driver.findElement(By.name("ctl00$MainContent$txtFirstName")).sendKeys("Apple Tree");
        driver.findElement(By.id("MainContent_txtEmail")).sendKeys("AppleTree@outlook.com");
        driver.findElement(By.xpath("//*[@id='MainContent_txtHomePhone']")).sendKeys("0455677645");
        driver.findElement(By.cssSelector("input[type='password'][id='MainContent_txtPassword']")).sendKeys("thispass");
        driver.findElement(By.name("ctl00$MainContent$txtVerifyPassword")).sendKeys("thispass");

        driver.findElement(By.cssSelector("input[name='ctl00$MainContent$Gender'][value='Female']")).click(); //gender
        new Select(driver.findElement(By.id("MainContent_menuCountry"))).selectByVisibleText("Germany"); //country

        driver.findElement(By.name("ctl00$MainContent$checkMonthlyEmail")).click();
        driver.findElement(By.id("MainContent_btnSubmit")).click();

        // 4. Get confirmation
        String conf = driver.findElement(By.id("MainContent_lblTransactionResult")).getText();
        System.out.println("Confirmation: " + conf);

        // 5. Close the browser
        driver.close();
    }
}
