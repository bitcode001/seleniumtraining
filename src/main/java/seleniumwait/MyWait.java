package seleniumwait;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class MyWait {

    public static WebDriver driver;

    @BeforeMethod
    public static void createDriver() {
        driver = utilities.DriverFactory.open("chrome");
    }

    @AfterMethod
    public static void quitDriver() {
        driver.quit();
    }

    @Test
    public void clickStartButton_ExplicitWait() {
        driver.get("http://the-internet.herokuapp.com/dynamic_loading/2");
        waitForElementToBeVisible(By.cssSelector("#start>button"), 2);
        driver.findElement(By.cssSelector("#start>button")).click();
        waitForElementToBeVisible(By.id("finish"), 10);
        String textFromElement = driver.findElement(By.id("finish")).getText();
        assertThat(textFromElement, equalTo("Hello World"));
    }

    public void waitForElementToBeVisible(By selector, int timeToWaitInSeconds) {
        WebDriverWait wait = new WebDriverWait(driver, timeToWaitInSeconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
    }
}
